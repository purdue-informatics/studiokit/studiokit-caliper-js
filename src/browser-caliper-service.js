/*
	Copyright 2017 Purdue University

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

import * as Sentry from '@sentry/browser'
import { merge } from 'lodash'
import StandardHttpError from 'standard-http-error'
import BrowserStorageService from './browser-storage-service'
import CaliperService from './caliper-service'

//
// Options
//

function onError(error) {
	var shouldTrack = true
	if (error instanceof StandardHttpError) {
		if (error.code === 401) {
			shouldTrack = false
		}
	} else {
		try {
			// try to parse
			var errorJson = JSON.parse(error.message)
			if (errorJson && errorJson.status && errorJson.status === 401) {
				shouldTrack = false
			}
		} catch (err) {
			// ignore
		}
	}
	if (shouldTrack) {
		Sentry.captureException(error)
	}
}

function mergeOptions(options) {
	var defaults = {
		storageService: new BrowserStorageService(),
		onError: onError
	}
	return merge({}, defaults, options)
}

/*
 * Subclass of CaliperService.
 * Uses `@sentry/browser` for error handling and `BrowserStorageService` for storage service.
 */
export class BrowserCaliperService extends CaliperService {
	constructor(options) {
		// Override options
		options = mergeOptions(options)
		// Call superclass constructor
		super(options)

		this.onError = onError
		this.mergeOptions = mergeOptions
	}
}

export default BrowserCaliperService
