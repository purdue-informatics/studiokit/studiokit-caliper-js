import * as Sentry from '@sentry/browser'
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import chaiThings from 'chai-things'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import HttpError from 'standard-http-error'
import utils from '../test/utils'
import BrowserCaliperService from './browser-caliper-service'

chai.use(chaiThings)
chai.use(chaiAsPromised)
chai.use(sinonChai)
chai.should()

const expect = chai.expect

function stubSentryCaptureException() {
	return sinon.stub(Sentry, 'captureException').callsFake(function () {})
}

function restoreSentryCaptureException() {
	Sentry.captureException.restore()
}

describe('BrowserCaliperService', function () {
	describe('ctor', function () {
		it('succeeds', function () {
			expect(() => new BrowserCaliperService(utils.getOptions())).to.not.throw(Error)
		})
	})

	describe('onError', function () {
		let store
		let service
		let captureExceptionStub

		beforeEach(function () {
			store = {
				'studioKit:caliperService:queue': [{}, {}]
			}
			const options = utils.getOptions(store)
			service = new BrowserCaliperService(options)
			captureExceptionStub = stubSentryCaptureException()
		})

		afterEach(function () {
			restoreSentryCaptureException()
		})

		describe('with HttpError 401', function () {
			beforeEach(function () {
				utils.stubCaliperSensorSend401()
			})

			afterEach(function () {
				utils.restoreCaliperSensorSend()
			})

			it('does not call Sentry.captureException', function () {
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.callCount(0)
				})
			})
		})

		describe('with non-401 HttpError', function () {
			const error = new HttpError(400)
			error.data = {
				userId: 1
			}
			beforeEach(function () {
				utils.stubCaliperSensorSendError(error)
			})

			afterEach(function () {
				utils.restoreCaliperSensorSend()
			})

			it('calls Sentry.captureException', function () {
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.callCount(1)
				})
			})

			it('calls Sentry.captureException with error and error.data', function () {
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.been.calledWith(error)
				})
			})
		})

		describe('with generic Error', function () {
			const error = new Error()
			error.data = {
				userId: 1
			}
			beforeEach(function () {
				utils.stubCaliperSensorSendError(error)
			})

			afterEach(function () {
				utils.restoreCaliperSensorSend()
			})

			it('calls Sentry.captureException', function () {
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.callCount(1)
				})
			})

			it('calls Sentry.captureException with error and error.data', function () {
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.been.calledWith(error)
				})
			})
		})

		describe('with generic Error where message is JSON and has status', function () {
			afterEach(function () {
				utils.restoreCaliperSensorSend()
			})

			it('does not call Sentry.captureException if status = 401', function () {
				utils.stubCaliperSensorSendError(new Error('{"status":401}'))
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.callCount(0)
				})
			})

			it('calls Sentry.captureException if status != 401', function () {
				utils.stubCaliperSensorSendError(new Error('{"status":400}'))
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.callCount(1)
				})
			})

			it('calls Sentry.captureException with error and error.data if status != 401', function () {
				const error = new Error('{"status":400}')
				error.data = {
					userId: 1
				}
				utils.stubCaliperSensorSendError(error)
				return service.send().catch(() => {
					return expect(captureExceptionStub).to.have.been.calledWith(error)
				})
			})
		})
	})
})
