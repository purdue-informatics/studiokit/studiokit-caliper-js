import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import chaiThings from 'chai-things'
import sinonChai from 'sinon-chai'
import BrowserStorageService from './browser-storage-service'

chai.use(chaiThings)
chai.use(chaiAsPromised)
chai.use(sinonChai)
chai.should()

const expect = chai.expect

describe('BrowserStorageService', function () {
	let service

	describe('ctor', function () {
		it('succeeds', function () {
			expect(() => (service = new BrowserStorageService())).to.not.throw(Error)
		})
	})
	describe('setItem', function () {
		it('succeeds', function () {
			expect(() => service.setItem('foo', 'bar')).to.not.throw(Error)
		})
		it('succeeds with Plain Object', function () {
			expect(() =>
				service.setItem('obj', {
					blah: 'blah'
				})
			).to.not.throw(Error)
		})
	})
	describe('getItem', function () {
		it('succeeds to get simple value', function () {
			let value
			expect(() => (value = service.getItem('foo'))).to.not.throw(Error)
			expect(value).to.equal('bar')
		})
		it('succeeds to get object', function () {
			let value
			expect(() => (value = service.getItem('obj'))).to.not.throw(Error)
			expect(value).to.deep.equal({
				blah: 'blah'
			})
		})
	})
	describe('removeItem', function () {
		it('succeeds to remove simple value', function () {
			expect(() => service.removeItem('foo')).to.not.throw(Error)
			const value = service.getItem('foo')
			expect(value).to.be.null
		})
		it('succeeds to remove object', function () {
			expect(() => service.removeItem('obj')).to.not.throw(Error)
			const value = service.getItem('obj')
			expect(value).to.be.null
		})
	})
})
