import { Dictionary } from 'lodash'

export interface CaliperToken {
	accessToken: string
	expires: string
}

export interface CaliperSensorOptions {
	hostname: string
	protocol: string
	port: string
	path: string
	method: string
}

export interface CaliperStorageService {
	getItem: (key: string) => any
	setItem: (key: string, value: any) => void
	removeItem: (key: string) => void
}

export interface CaliperServiceOptions {
	sensorId: string | undefined | null
	sensorOptions: CaliperSensorOptions | undefined | null
	appId: string | undefined | null
	appName: string | undefined | null
	getToken: () => Promise<CaliperToken | undefined> | null
	storageService: CaliperStorageService | null
	autoSend: boolean
	sendInterval: number
	sessionIriPrefix: string | null
	sessionTimeoutThreshold: number
	sessionKeepAliveThreshold: number
	activityUpdateThreshold: number
	onError: (error: any) => void | null
}

export interface CaliperService {
	isInitialized: () => boolean
	getOrRefreshAuthToken: () => Promise<CaliperToken>
	addToQueue: (item: any) => void
	send: (maxItems?: number) => Promise<void>
	onActivity: (e: any) => void
	isMediaPlaying: () => boolean
	setPerson: (id?: string, firstName?: string, lastName?: string, extensions?: Dictionary<any>) => void
	startSession: (extensions?: Dictionary<any>) => void
	keepAliveSession: () => void
	endSession: (endedAtTime?: string, didTimeOut?: boolean) => void
	startAutoSendInterval: () => void
	clearAutoSendInterval: () => void
	setShouldClearAutoSendOnceExpired: (value: boolean) => void
}
