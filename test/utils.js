import Caliper from 'caliperjs'
import moment from 'moment'
import sinon from 'sinon'
import HttpError from 'standard-http-error'
import { v4 as uuid } from 'uuid'

function getOptions(store) {
	store = store || {}
	return {
		sensorId: 'https://app.example.edu/sensor',
		sensorOptions: {
			protocol: 'https:',
			hostname: 'localhost',
			port: '3001',
			path: '/collector',
			method: 'POST'
		},
		appId: 'https://app.example.edu',
		appName: 'Example App',
		getToken: function () {
			return new Promise(function (resolve) {
				resolve({
					accessToken: uuid(),
					expires: moment().add(2, 'hours').toISOString()
				})
			})
		},
		storageService: {
			getItem: function (key) {
				return store[key]
			},
			setItem: function (key, value) {
				store[key] = value
			},
			removeItem: function (key) {
				delete store[key]
			}
		}
	}
}

function stubCaliperSensorSend() {
	sinon.stub(Caliper.Sensor, 'send').callsFake(function () {
		return new Promise(function (resolve) {
			setTimeout(function () {
				resolve()
			}, 20)
		})
	})
}

function stubCaliperSensorSendError(error) {
	sinon.stub(Caliper.Sensor, 'send').callsFake(function () {
		return new Promise(function (resolve, reject) {
			setTimeout(function () {
				reject(error)
			}, 20)
		})
	})
}

function stubCaliperSensorSend401() {
	stubCaliperSensorSendError(new HttpError(401))
}

function stubCaliperSensorSend400() {
	stubCaliperSensorSendError(new HttpError(400))
}

function restoreCaliperSensorSend() {
	Caliper.Sensor.send.restore()
}

export default {
	getOptions,
	stubCaliperSensorSend,
	stubCaliperSensorSendError,
	stubCaliperSensorSend401,
	stubCaliperSensorSend400,
	restoreCaliperSensorSend
}
